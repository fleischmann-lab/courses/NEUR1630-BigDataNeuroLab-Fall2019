"""Create a volume from SWC file"""
__title__ = "NeuronCAD"
__author__ = "Andrea PIERRÉ"
__date__ = "2019/12/04"
__version__ = "1.0.0"

from FreeCAD import Base
import Draft, Part


FILEPATH = "C:\\Users\\apierre3\\Desktop\\3D-Brain\\neur1630\\DemoData\\AA0049.swc"


def create_cylinder(radius, X0, Y0, Z0, X1, Y1, Z1):
    """Function to create a cylinder"""
    circ = Part.makeCircle(current_radius, Base.Vector(X0, Y0, Z0))
    circ_wire = Part.Wire(circ)
    disc = Part.Face(circ_wire)
    cylinder = disc.extrude(Base.Vector(X1 - X0, Y1 - Y0, Z1 - Z0))
    return cylinder


with open(FILEPATH, "r") as file:
    SampleNumber = []
    StructureIdentifier = []
    X = []
    Y = []
    Z = []
    radius = []
    ParentSample = []

    for row in file:
        if row.strip()[0] == "#":  # Do not parse comments
            continue
        item = row.split("\t")

        # See http://www.neuronland.org/NLMorphologyConverter/MorphologyFormats/SWC/Spec.html
        SampleNumber.append(int(item[0]))
        StructureIdentifier.append(item[1])
        X.append(float(item[2]))
        Y.append(float(item[3]))
        Z.append(float(item[4]))
        radius.append(float(item[5]))
        ParentSample.append(int(item[6]))
        # print(SampleNumber, " ", StructureIdentifier, " ", X, " ", Y," ", Z, " ", radius, " ", ParentSample)

    for idx, val in enumerate(SampleNumber):
        # current_radius = radius[idx]
        current_radius = 5

        if (
            SampleNumber[idx] == ParentSample[idx] + 1
        ):  # Case if the points are continuous (same branch)
            cylinder = create_cylinder(
                current_radius,
                X[idx],
                Y[idx],
                Z[idx],
                X[idx + 1],
                Y[idx + 1],
                Z[idx + 1],
            )
            if idx == 0:
                volume_union = cylinder
            volume_union = volume_union.fuse(cylinder)

        else:  # Case if it starts a new branch

            # Create a cylinder at the start of the branch
            circ = Part.makeCircle(
                current_radius,
                Base.Vector(
                    X[ParentSample[idx]], Y[ParentSample[idx]], Z[ParentSample[idx]]
                ),
            )
            circ_wire = Part.Wire(circ)
            disc = Part.Face(circ_wire)
            cylinder = disc.extrude(
                Base.Vector(
                    X[idx] - X[ParentSample[idx]],
                    Y[idx] - Y[ParentSample[idx]],
                    Z[idx] - Z[ParentSample[idx]],
                )
            )
            cylinder = create_cylinder(
                current_radius,
                X[ParentSample[idx]],
                Y[ParentSample[idx]],
                Z[ParentSample[idx]],
                X[idx],
                Y[idx],
                Z[idx],
            )
            if idx == 0:
                volume_union = cylinder
            volume_union = volume_union.fuse(cylinder)

            cylinder = create_cylinder(
                current_radius,
                X[idx],
                Y[idx],
                Z[idx],
                X[idx + 1],
                Y[idx + 1],
                Z[idx + 1],
            )
            volume_union = volume_union.fuse(cylinder)

    Part.show(volume_union)

App.ActiveDocument.recompute()
