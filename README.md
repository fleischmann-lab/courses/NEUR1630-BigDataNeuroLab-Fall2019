# neur1630

To convert a SWC file from http://ml-neuronbrowser.janelia.org/ to a 3D volume, 
you can paste the from `code > FreeCADMacro.py` in a FreeCAD macro and run it.

_Don't forget to change the `FILEPATH` variable to point to your SWC file path._